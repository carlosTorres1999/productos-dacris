$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  $('.carousel').carousel({
    interval: 2000
  });

  $('#exampleModal').on('show.bs.modal', (e) => {
    console.log('Show modal event');
    $('#suscribeBTN').removeClass('btn-primary');
    $('#suscribeBTN').addClass('btn-secondary');
    $('#suscribeBTN').prop('disabled', true);

  } );

  $('#exampleModal').on('shown.bs.modal', (e) => {
    console.log('Shown modal event')
  } )

  $('#exampleModal').on('hide.bs.modal', (e) => {
    console.log('Shown modal event')
  } )

  $('#exampleModal').on('hidden.bs.modal', (e) => {
    console.log('hidden modal event')
    $('#suscribeBTN').removeClass('btn-secondary');
    $('#suscribeBTN').addClass('btn-primary');
    $('#suscribeBTN').prop('disabled', false);
  } )